#include <map>

#include "text.h"

class Config {
public:
    Config() = default;

    template <typename T> void setArray(const std::string& key, const std::vector<T>& val) {
        std::string str;
        for(T el : val) {
            str += corryvreckan::to_string(el);
            str += ",";
        }
        str.pop_back();
        config_[key] = str;
    }
private:
    std::map<std::string, std::string> config_;
};


int main() {

    Config conf;
    conf.setArray<bool>("test", {true, false, true});
    return 0;
}


